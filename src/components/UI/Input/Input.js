import React, { memo } from 'react'

const Input = ({input, label, type,  meta: {error, touched}}) => {

  return (
    <div className="form-control">
      <label 
        className="form-control__label" 
        htmlFor={input.name}
      >
        {label}
      </label>
      <input 
        {...input} 
        autoComplete="off" 
        className="form-control__input"  
        type={type} 
        id={input.name} 
      />
       {touched && error && <span className="form-control__error">{error}</span>}
    </div>
  )
}

export default memo(Input)