import React, {Component, Fragment} from 'react';
import CloseButton from '../UI/CloseButton/CloseButton';
import PropTypes from 'prop-types'


class Modal extends Component {
  render() {
    const cls = ['modal', `modal--${this.props.type}`];
    const footerCls = ['modal-footer', this.props.actions.length === 1 ? 'modal-footer--single': '']

    return (
      <Fragment>
        <div className={cls.join(' ')}>
          <div className="modal-header">
            <p className='modal-header__text'>
              {this.props.header}
            </p>
            {this.props.closeButton && <CloseButton onCLick={this.props.closeHandler}/>}
          </div>

          <div className="modal-content">
            {this.props.text}

          </div>

          <div className={footerCls.join(' ')}>
            {this.props.actions}
          </div>
        </div>
        <div className={'backdrop'} onClick={this.props.closeHandler}>

        </div>
      </Fragment>
    );
  }
}

Modal.propTypes = {
  type: PropTypes.string,
  header: PropTypes.string,
  text: PropTypes.string,
  actions: PropTypes.array,
  closeButton: PropTypes.bool,
  closeHandler: PropTypes.func,
};

export default Modal;