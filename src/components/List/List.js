import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Product from '../Product/Product';
import Modal from '../Modal/Modal';
import Button from '../UI/Button/Button';
import { setProduct } from '../../store/actions/productActions';
import { toggleModal } from '../../store/actions/modalActions'

import './List.scss'

const List = ({ products }) => {
  const dispatch = useDispatch()
  const currentProductId = useSelector(state => state.modal.currentProductId)
  const isModal = useSelector(state => state.modal.cartModal)

  const handleModalSubmit = (id) => {
    dispatch(setProduct('cart', id));
    dispatch(toggleModal());
  }

  const modal = 
  <Modal
    header={'Подтверждение'}
    text={`Вы уверены, что хотите добавить товар в корзину?`}
    closeButton={true}
    closeHandler={() => dispatch(toggleModal())}
    type={'submit'}
    actions={
      [<Button
      autoFocus
      key={1}
      text={'ОК'}
      onClick={() => handleModalSubmit(currentProductId)}
      backgroundColor={'red'}
      />]
    }
  />

  return (
    <>
      <div className='list'>
        {products.map((product, index) => {
          return (
            <Product
              key={product.id}
              name={product.name}
              id={product.id}
              price={product.price}
              src={product.image_url}
              isInCart={product.isInCart}
              isFavorite={product.isFavorite}
              setToCart={() => dispatch(toggleModal(product.id))}
              setToFavorites={() => dispatch(setProduct('favorite', product.id))}
            />)
        })}
      </div>
      {isModal && modal}
    </>
  )
};


export default List;
