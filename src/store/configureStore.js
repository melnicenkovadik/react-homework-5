import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { rootReducer } from './reducers/rootReducer'
import { localStorageMiddleware  } from './middlewares/localStorageMiddleware'

const composeEnhancers = composeWithDevTools({})

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk, localStorageMiddleware)));
