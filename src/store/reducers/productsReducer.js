 import {
  GET_PRODUCTS,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_ERROR,
  SET_PRODUCT_TO_CART,
  SET_PRODUCT_TO_FAVORITES,
  SET_PRODUCT_COUNT,
  CLEAR_CART,
} from '../actions/productActions'


const initialState = {
  products: [],
  isFetching: false,
  error: false,
  currentProductId: null,
}

export const productsReducer = (state = initialState, { type, payload }) => {
  switch (type) {

    case GET_PRODUCTS:
      return {
        ...state,
        isFetching: true,
        error: false,
      }
    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        products: payload,
      }
    case GET_PRODUCTS_ERROR:
      return {
        ...state,
        isFetching: false,
        error: true
      }

    case SET_PRODUCT_TO_CART:
      return {
        ...state,
        products: state.products.map(prod => {
          return prod.id === payload  
          ? {
            ...prod,
            isInCart: !prod.isInCart,
            count: prod.isInCart ? 0 : 1,
            }
          : prod
        })
      }

    case SET_PRODUCT_TO_FAVORITES:
      return {
        ...state,
        products: state.products.map(prod => {
          return prod.id === payload  
          ? {
            ...prod,
            isFavorite: !prod.isFavorite
            }
          : prod
        })
      }
    
    case SET_PRODUCT_COUNT : 
      return {
        ...state,
        products: state.products.map(prod => {
          return prod.id === payload.id 
          ? {
            ...prod,
            count: payload.count > 0 ? payload.count : 1
            }
          : prod
        })
      }

      case CLEAR_CART : {
        return {
          ...state,
          products: state.products.map(prod => prod.isInCart = false) 
        }
      }
    default:
      return state
  }
}
