export const GET_CART_PRODUCTS = 'GET_CART_PRODUCTS';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART ';
export const SET_PRODUCT_COUNT = 'SET_PRODUCT_COUNT'

export const getCartProducts = () => {
  const cartProducts = JSON.parse(localStorage.getItem('cart'));
  return {
    type: GET_CART_PRODUCTS,
    payload: cartProducts,
  }
};

export const setProductCount = (id, count) => ({
  type: SET_PRODUCT_COUNT,
  payload: {id, count}
})


export const removeItemFromCart = id => ({
  type: REMOVE_FROM_CART,
  payload: id
});

